
pb:
	protoc -I v1 -I third_party --go_out=plugins=grpc:${GOPATH}/src ./v1/*.proto 
	protoc -I v1 -I third_party --grpc-gateway_out=logtostderr=true:${GOPATH}/src ./v1/*.proto 
deps: 
	go install github.com/golang/protobuf/protoc-gen-go@latest
	go install  github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@latest
	go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-openapiv2@latest


pub-api:
	git clone git@gitlab.com:vinothsha/types.git go-build
	make pb;
	cd go-build && git add . && git commit -m "types-generated" && git push && cd .. || true
	make clean

clean:
	rm -rf go-build

gitpush:
	git add . && git commit -m "updated protos" && git push

#mkdir -p google/api
# go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@latest
# go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
#curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > google/api/annotations.proto
#curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > google/api/http.proto

#sudo apt install golang-goprotobuf-dev
# export PS1='[\u@\h \W]\$'
# export PATH=$PATH:/usr/local/go/bin
# export GOROOT=/usr/local/go
# export GOAPTH=/home/sha/go

# git config --global credential.helper store
# git config --global credential.https://gitlab.com.access-token <your-access-token>


# git config --global --unset credential.https://gitlab.com.helper
# git config --global --unset credential.https://gitlab.com.access-token
